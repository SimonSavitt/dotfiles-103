#!/bin/bash

i3 &
mode="1366x768"
[[ -f ~/.config/x11mode ]] && mode=$(head -1 ~/.config/x11mode)
sleep 3 && xrandr --output VGA-1 --mode "$mode"
feh --bg-scale ~/Desktop/wallpaper/bamboo4.jpg
wait
